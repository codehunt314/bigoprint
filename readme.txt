Custom photo framing solution app. 

Setting it up:
	1- virtualenv virt_env
	2- In settings.py file
		I- configure db settings
		II- configure your sendgrid username/password for sending emails
	3- python manage.py syncdb
	4- python manage.py runserver 8000, go to http://127.0.0.1:8000/
	

It provide following functionality to user:


1- 	High Quality poster size print of your artwork

	By simply Uploading your artwork (high resolution images of Paintings) or photos (family photos, baby photos, sceneries and more) and Choose the dimension (size) of your print.


2- 	Frame these artwork

	Once you have selected the size of printable image, you can frame it in one go. App provide you the online tool to choose the type/size of Frame, border size/color and color of your wall. These options will help you visualize, how the artwork will appear in your home.

3- 	Place an Order

	After finalising the frame, all you need to do is place an order and make payment.
