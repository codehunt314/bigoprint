from django.shortcuts import render
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, render_to_response
from django.template import RequestContext, loader

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from customer.models import *
from orderapp.models import PrintOrder, OrderStatus, OrderPGResponse, ORDER_STATUS
from post_office import mail
from django.contrib.auth.tokens import default_token_generator

import random, string, base64, json
from django.template.loader import render_to_string


def customer_registration(request):
	if request.method == "POST":
		email 				= 	request.POST.get('email')
		password1 			= 	request.POST.get('password1')
		contact_no 			=	request.POST.get('contact_no', None)
		redirect_url 		=	request.POST.get('redirect_url', None)
		user 				= 	User.objects.create_user(email, email, password1)
		customer, created 	=	Customer.objects.get_or_create(user=user, defaults={'contact_no' : contact_no})
		return HttpResponseRedirect(redirect_url)
	else:
		return render_to_response('register.html', {}, context_instance=RequestContext(request))


def add_address(request):
	if not request.user.id:
		return HttpResponseRedirect('/customer/login/')
	if request.method=='GET':
		return HttpResponseRedirect('/orders/')
	customer 	=	Customer.objects.get(user=request._cached_user)
	addressid 	= 	request.POST.get('addressid', None)
	if addressid:
		addr = Address.objects.get(id=addressid)
		cust_addr, created 	=	CustomerAddress.objects.get_or_create(customer=customer, address=addr)
	else:
		name 		=	request.POST.get('name', None)
		mobile 		=	request.POST.get('mobile', None)
		address 	=	request.POST.get('address', None)
		city 		=	request.POST.get('city', 'Mumbai')
		country 	=	request.POST.get('country', 'India')
		pincode 	=	request.POST.get('pincode', None)
		landmark 	=	request.POST.get('landmark', None)
		addr 		=	Address.objects.create(fullname=name, contact_no=mobile, address=address, city=city, country=country, pincode=pincode, landmark=landmark)
		cust_addr, created 	=	CustomerAddress.objects.get_or_create(customer=customer, address=addr)
	orderid 	=	request.POST.get('orderid')
	if orderid:
		printorder 	=	PrintOrder.objects.get(id=orderid)
		printorder.ordered_by 	=	customer
		printorder.address 		= 	cust_addr
		printorder.save()
		printorder.update_estimated_time()
	request.session['addr_saved']	=	True
	return HttpResponseRedirect('/order/%s/'%(orderid))


def edit_address(request, addressid):
	if not request.user.id:
		return HttpResponseRedirect('/customer/login/')
	if request.method=='GET':
		return HttpResponseRedirect('/orders/')
	addr 			=	Address.objects.get(id=addressid)
	addr.fullname 	=	request.POST.get('name', None)
	addr.contact_no =	request.POST.get('mobile', None)
	addr.address 	=	request.POST.get('address', None)
	addr.city 		=	request.POST.get('city', 'Mumbai')
	addr.country 	=	request.POST.get('country', 'India')
	addr.pincode 	=	request.POST.get('pincode', None)
	addr.landmark 	=	request.POST.get('landmark', None)
	addr.save()
	return render_to_response('address-block.html', {'address':addr}, context_instance=RequestContext(request))
	# return HttpResponse('address saved')


def delete_address(request, addressid):
	if not request.user.id:
		return HttpResponseRedirect('/customer/login/')
	addr 			=	Address.objects.get(id=addressid)
	cust_addr 		=	CustomerAddress.objects.get(address=addr)
	PrintOrder.objects.filter(address=cust_addr).update(address=None)
	cust_addr.delete()
	addr.delete()
	return HttpResponse('address deleted')


def _send_welcome_email_with_password(user, password, password_reset_link=None):
	rendered_str = render_to_string('welcome_withpassword.html', {'password': password})
	mail.send(
				[user.email],
				'contact@framd.in',
				subject='Welcome to Framd.in',
				message=rendered_str,
				html_message=rendered_str,
				headers={'Reply-to': 'contact@framd.in'},
				context={'password': password, 'password_reset_link': password_reset_link, 'username': user.email.split('@')[0]},
				priority='now',
			)

def customer_login(request):
	if request.method=='POST':
		email 			= 	request.POST.get('email')
		password 		= 	request.POST.get('password')
		redirect_url 	=	request.POST.get('redirect_url', '/')
		if not password:
			try:
				user 	=	User.objects.get(username=email)
				return render_to_response('login.html', {
					'error' : 'User is already registered.',
					'email' : email,
					'password' : password,
					'redirect_url': redirect_url
					}, context_instance=RequestContext(request))
			except:
				password 			=	''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8)) #password length
				user 				= 	User.objects.create_user(email, email, password)
				customer, created 	=	Customer.objects.get_or_create(user=user)
				user 				= 	authenticate(username=email, password=password)
				### send welcome email, along with password# ##
				token 		=	default_token_generator.make_token(user)
				uidb64 		=	base64.urlsafe_b64encode(user.email)
				reset_link 	=	'http://%s/customer/reset_password/%s/%s/'%( request.get_host(), uidb64, token)
				_send_welcome_email_with_password(user, password, reset_link)
		else:
			user 		= 	authenticate(username=email, password=password)
		if user is not None:
			if True or user.is_active: 	# no provision of activation/deactivation
				login(request, user)
				return HttpResponseRedirect(redirect_url)
			else:
				return HttpResponse('disabled account')
		else:
			return render_to_response('login.html', {
				'error' : 'Incorrect login details',
				'email' : email,
				'password' : password,
				'redirect_url': redirect_url
				}, context_instance=RequestContext(request))
			return HttpResponse('invalid login')
	else:
		redirect_url 	=	request.GET.get('redirect_url', '/')
		return render_to_response('login.html', {'redirect_url': redirect_url}, context_instance=RequestContext(request))


def customer_logout(request):
	logout(request)
	return HttpResponseRedirect('/')


def forgot_password(request):
	if request.method=='POST':
		email 	=	request.POST.get('email')
		try:
			user 	= 	User.objects.get(email=email)
			token 	=	default_token_generator.make_token(user)
			uidb64 	=	base64.urlsafe_b64encode(user.email)
			mail.send(
				[user.email],
				'contact@framd.in',
				subject='Welcome!',
				message="Reset your password,  <a href='{{reset_link}}'>click this link</a>!",
				html_message="Reset your password,  <a href='{{reset_link}}'>click this link</a>!",
				headers={'Reply-to': 'contact@framd.in'},
				context={'reset_link': 'http://%s/customer/reset_password/%s/%s/'%( request.get_host(), uidb64, token)},
				priority='now',
			)
			return render_to_response('forgot_password.html', {'email':email, 'success': 'Sent password reset email to your email, Check your email'}, context_instance=RequestContext(request))
		except:
			return render_to_response('forgot_password.html', {'email':email, 'error': 'Email address is not registered'}, context_instance=RequestContext(request))
	else:
		return render_to_response('forgot_password.html', {}, context_instance=RequestContext(request))


def reset_password(request, uidb64=None, token=None):
	if request.method=='POST':
		user_email	=	base64.urlsafe_b64decode(str(uidb64))
		user 		=	User.objects.get(email=user_email)
		password1 	=	request.POST.get('password1')
		password2 	=	request.POST.get('password2')
		check_token =	default_token_generator.check_token(user, token)
		if not check_token:
			return HttpResponse('invalid password reset token')
		if password1==password2:
			user.set_password(password1)
			user.save()
			return HttpResponseRedirect('/customer/login/')
		return HttpResponse('password are not matching')
	else:
		return render_to_response('reset_password.html', {}, context_instance=RequestContext(request))


def feedback(request):
	if request.method=='POST':
		feeback_obj, created 	=	Feedback.objects.get_or_create(
										name 		=	request.POST.get('name'),
										email 		=	request.POST.get('email'),
										contact_no 	=	request.POST.get('number'),
										message 	=	request.POST.get('message'),
									)
		# return HttpResponse('Thanks for contacting us.')
	return HttpResponseRedirect('/contact-us/')


def admin_dashboard(request):
	if not request.user.id:
		return HttpResponseRedirect('/customer/login/?redirect_url=%s'%(request.path))
	if not (request.user.is_superuser or request.user.is_staff):
		return HttpResponse('Priveledge section, try again!!')
	if request.method=='GET':
		order_list 	=	[]
		for order in PrintOrder.objects.all().order_by('-ordered_time'):
			order_status_list, current_status = order.order_status()
			order_list.append({
				'orderid' : order.id,
				'price': 'INR ' + str(float(order.offered_price)),
				'order_date': order.ordered_time.strftime('%d %b \'%y (%I:%M %p)'),
				'order_by_name': order.address.address.fullname if order.address else '',
				'shipping_address': order.address.address.get_address() if order.address else '',
				'contact_no': order.address.address.contact_no if order.address else '',
				'order_status': order_status_list,
				'current_status': current_status,
				'payment_status': order.payment_status(),
				'media_file': order.photo.imagefile.url
			})

		return render_to_response('admin_dashboard.html', {
			'orders': order_list,
			'order_statuses': ORDER_STATUS
		}, context_instance=RequestContext(request))
	else:
		print_order 	=	PrintOrder.objects.get(id=request.POST.get('orderid'))
		order_list 	=	[]
		order_status_list, current_status = print_order.order_status()
		order_list.append({
				'orderid' : print_order.id,
				'price': 'INR ' + str(float(print_order.offered_price)),
				'order_date': print_order.ordered_time.strftime('%d %b \'%y (%I:%M %p)'),
				'order_by_name': print_order.address.address.fullname if print_order.address else '',
				'shipping_address': print_order.address.address.get_address() if print_order.address else '',
				'contact_no': print_order.address.address.contact_no if print_order.address else '',
				'order_status': order_status_list,
				'current_status': current_status,
				'payment_status': print_order.payment_status(),
				'media_file': print_order.photo.imagefile.url,
			})
		return render_to_response('admin_dashboard.html', {'orders': order_list}, context_instance=RequestContext(request))

def admin_set_status(request):
	if not request.user.id:
		return HttpResponseRedirect('/customer/login/?redirect_url=%s'%(request.path))
	if not (request.user.is_superuser or request.user.is_staff):
		return HttpResponse('Priveledge section, try again!!')
	if request.method=='POST':
		orderid = 	request.POST.get('orderid', None)
		status 	=	request.POST.get('status', None)
		if orderid and status:
			porder 	=	PrintOrder.objects.get(id=orderid)
			order_status, created 	= 	OrderStatus.objects.get_or_create(order=porder, status=status)
			return HttpResponseRedirect('/admin_dashboard/')
	return HttpResponseRedirect('/admin_dashboard/')

