from django.db import models
from django.contrib.auth.models import User

class Address(models.Model):
	fullname 	=	models.CharField(max_length=255, blank=True, null=True)
	address 	=	models.TextField(blank=True, null=True)
	city 		=	models.CharField(max_length=255, blank=True, null=True)
	state 		=	models.CharField(max_length=255, blank=True, null=True)
	country 	=	models.CharField(max_length=255, default='India')
	pincode 	=	models.CharField(max_length=6, blank=True, null=True)
	contact_no 	=	models.CharField(max_length=13, blank=True, null=True)
	landmark 	=	models.TextField(blank=True, null=True)

	def get_address(self):
		landmark_str 	=	'Landmark: '+self.landmark if self.landmark else ''
		return '%s\n%s\n%s-%s, %s, %s\n Contact No: %s\n%s' % (self.fullname, self.address, self.city, self.pincode, self.state, self.country, self.contact_no, landmark_str)


class Customer(models.Model):
	user 		= 	models.OneToOneField(User)
	contact_no	=	models.CharField(max_length=13, blank=True, null=True)
	profile_pic	=	models.ImageField(upload_to='uploaded/profile_pic', blank=True, null=True)


class CustomerAddress(models.Model):
	customer	=	models.ForeignKey(Customer, blank=True, null=True)
	address 	=	models.ForeignKey(Address, blank=True, null=True)


class Feedback(models.Model):
	name 		=	models.CharField(max_length=255, blank=True, null=True)
	email 		=	models.CharField(max_length=255, blank=True, null=True)
	contact_no 	=	models.CharField(max_length=255, blank=True, null=True)
	message 	=	models.TextField(blank=True, null=True)
	submit_time =	models.DateTimeField(auto_now=False, auto_now_add=True)

