from django.db import models
from decimal import Decimal
import random, string, math, json

from customer.models import Customer, CustomerAddress, Address
from datetime import datetime, timedelta

def id_generator(size=20, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _x in range(size))


def _unique_model_id(model_instance):
    value = str(id_generator(size=8))
    while model_instance.__class__.objects.filter(id=value).exists():
        value = str(id_generator(size=8))
    return value


class ArbitIDField(models.CharField):
    def __init__(self, *args, **kwargs):
        models.CharField.__init__(self, *args, **kwargs)
    
    def pre_save(self, model_instance, add):
        if add :
            value = _unique_model_id(model_instance)
            setattr(model_instance, self.attname, value)
            return value
        else:
            return super(models.CharField, self).pre_save(model_instance, add)


def uploaddir(instance, filename):
	upload_date 	=	instance.upload_time.date().strftime('%d-%b-%y(%a)')
	return '/'.join(['uploaded', 'order', upload_date, filename])


class Photo(models.Model):
	imagefile 	=	models.ImageField(upload_to=uploaddir, width_field='x_pixel', height_field='y_pixel')
	upload_time	=	models.DateTimeField(auto_now=False, auto_now_add=True)
	x_pixel 	=	models.PositiveIntegerField(blank=True, null=True)
	y_pixel 	=	models.PositiveIntegerField(blank=True, null=True)
	cost 		=	models.DecimalField(max_digits=9, decimal_places=2, default=Decimal('0000000.00'))
	uploaded_by	=	models.ForeignKey(Customer, blank=True, null=True)


class PrintType(models.Model):
	name 		=	models.CharField(max_length=255)
	cost 		=	models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('10.00'))
	unit 		=	models.CharField(max_length=255, default='sqft')

def update_print_types():
	print_choices 	=	[
							('Self Adhesive Filter (GLOSSY)', 60.00), 
							('Self Adhesive Filter (MAT)', 60.00), 
							('Photo Paper (GLOSSY)', 60.00), 
							('Photo Paper (MAT)', 60.00), 
							('Eco Vinyl (GLOSSY)', 40.00),
							('Eco Vinyl (MAT)', 40.00)
						]
	for name, cost in print_choices:
		PrintType.objects.get_or_create(name=name, cost=cost)



class Frame(models.Model):
	name 		=	models.CharField(max_length=255, blank=True, null=True)
	colorcode	=	models.CharField(max_length=255, blank=True, null=True)
	image 		=	models.ImageField(upload_to='uploaded/frame', blank=True, null=True)
	corner_size	=	models.CharField(max_length=255, blank=True, null=True) 	# setting to set border-image


class FramePreferences(models.Model):
	frame 		=	models.ForeignKey(Frame)
	thickness 	=	models.DecimalField(max_digits=3, decimal_places=2, default=Decimal('0.00'))
	cost 		=	models.DecimalField(max_digits=5, decimal_places=2, default=Decimal('10.00'))
	unit 		=	models.CharField(max_length=255, default='inch')
	

class Cardboard(models.Model):
	name 		=	models.CharField(max_length=255, blank=True)
	colorcode 	=	models.CharField(max_length=255, default='#fafafa')
	image 		=	models.ImageField(upload_to='uploaded/cardboard/', blank=True, null=True)
	cbcost		=	models.DecimalField(max_digits=5, decimal_places=2, default=Decimal('50.00'))
	cbcost_unit	=	models.CharField(max_length=255, default='sqft')


def update_cardboard():
	cardboard_choices 	=	[
								('White', 'ffffff', 50.00), 
								('Black', '000000', 50.00), 
								
							]
	for name, colorcode, cost in cardboard_choices:
		Cardboard.objects.get_or_create(name=name, colorcode=colorcode, cbcost=cost)


class PrintOrder(models.Model):
	id              =   ArbitIDField(max_length=50, primary_key=True, editable=False)
	photo 			=	models.ForeignKey(Photo)
	print_type 		=	models.ForeignKey(PrintType, blank=True, null=True)
	print_width 	=	models.DecimalField(max_digits=4, decimal_places=2, default=Decimal('0.00'))
	print_height 	=	models.DecimalField(max_digits=4, decimal_places=2, default=Decimal('0.00'))
	final_width 	=	models.DecimalField(max_digits=4, decimal_places=2, default=Decimal('0.00'))
	final_height 	=	models.DecimalField(max_digits=4, decimal_places=2, default=Decimal('0.00'))
	frame			=	models.ForeignKey(FramePreferences, blank=True, null=True)
	cardboard		=	models.ForeignKey(Cardboard, blank=True, null=True)
	cardboard_thickness 	=	models.DecimalField(max_digits=3, decimal_places=2, default=Decimal('0.00'))
	with_glass		=	models.BooleanField(default=True)
	wall_color_bg 	=	models.CharField(max_length=255, default='#aaa')
	ordered_by		=	models.ForeignKey(Customer, blank=True, null=True)
	ordered_time	=	models.DateTimeField(auto_now=True, auto_now_add=True)
	address 		=	models.ForeignKey(CustomerAddress, blank=True, null=True)
	total_cost		=	models.DecimalField(max_digits=9, decimal_places=2, default=Decimal('0.00'))
	offered_price 	=	models.DecimalField(max_digits=9, decimal_places=2, default=Decimal('0.00'))
	estimated_date 	= 	models.DateField(auto_now=False, auto_now_add=True, default='2015-06-30')

	def update_estimated_time(self):
		self.estimated_date = (self.ordered_time + timedelta(10)).date()
		self.save()

	def calculate_cost(self):
		total_cost		=	0.0
		print_rate		=	float(self.print_type.cost) if self.print_type else 60.0
		mat_rate 		=	0.0
		print_mat_cost 	=	float(self.print_width)/12.0 * float(self.print_height)/12.0 * (print_rate + mat_rate)
		print "Print cost: %s" %(print_mat_cost)
		if float(self.cardboard_thickness)>0.0:
			frame_rate 	=	float(self.frame.cost) + 3.00 	#mounting + glass + framing
		else:
			frame_rate 	=	float(self.frame.cost) + 2.00 	#glass + framing
		framing_cost 	=	2*(float(self.final_width) + float(self.final_height)) * frame_rate
		print "Framing cost: %s / %s" %(framing_cost, frame_rate)
		total_cost 		=	print_mat_cost + framing_cost
		if not self.with_glass:
			glass_cost 	=	0.25 * float(self.final_width) * float(self.final_width)
			total_cost 	=	total_cost - glass_cost
		print "Total cost: %s" %(total_cost)
		return math.floor(total_cost), math.floor(total_cost*1.25+150)
		# self.total_cost =	math.floor(total_cost)
		# self.offered_price 	=	math.floor(total_cost*1.25+150)
		# self.save()

	def set_cost(self):
		total_cost, offered_price 	=	self.calculate_cost()
		self.total_cost 			= 	total_cost
		self.offered_price 			= 	offered_price
		self.save()

	def order_status(self, in_string=True):
		results = []
		final_status = None
		for order in OrderStatus.objects.filter(order=self).order_by('status_time'):
			results.append((order.status_time.strftime('%d %b \'%y (%I:%M %p)'), order.status))
			final_status = order.status
		return results, final_status

	def payment_status(self, in_string=True):
		results = []
		for order_status in OrderStatus.objects.filter(order=self):
			for payment_status in OrderPGResponse.objects.filter(order_status=order_status):
				results.append((payment_status.status_time.strftime('%d %b \'%y (%I:%M %p)'), payment_status.get_transaction_info()))
		return results


ORDER_STATUS 	= 	(
						('Order Created', 'Order Created'),
						('Order Placed', 'Order Placed'),
						('Payment Done', 'Payment Done'),
						('Order Confirmed', 'Order Confirmed'),	#means Transaction Succussfully completed
						('Aborted', 'Aborted'),					#means Transaction Aborted
						('Failure', 'Failure'),					#means Transaction Failure
						('Invalid', 'Invalid'),					#means Transaction Invalid
						('In Processing', 'In Processing'),
						('In Transit', 'In Transit'),
						('Delivered', 'Delivered'),
						('Replacement', 'Replacement'),
						('Other', 'Other'),
					)


class OrderStatus(models.Model):
	order 			=	models.ForeignKey(PrintOrder)
	status_time		=	models.DateTimeField(auto_now=False, auto_now_add=True)
	status 			=	models.CharField(max_length=255, choices=ORDER_STATUS, default='Order Created')
	status_info		=	models.TextField(blank=True, null=True)


class OrderPGResponse(models.Model):
	order_status 	=	models.ForeignKey(OrderStatus)
	order_details 	=	models.TextField(blank=True, null=True)
	status_time		=	models.DateTimeField(auto_now=False, auto_now_add=True)

	def get_billing_address(self):
		order_detail_dict = json.loads(self.order_details)
		return '%s (%s)\n%s\n%s-%s, %s, %s\n%s' %(
				order_detail_dict['billing_name'], 
				order_detail_dict['billing_email'],
				order_detail_dict['billing_address'],
				order_detail_dict['billing_city'],
				order_detail_dict['billing_zip'],
				order_detail_dict['billing_state'],
				order_detail_dict['billing_country'],
				order_detail_dict['billing_tel'],
			)
	
	def get_shipping_address(self):
		order_detail_dict = json.loads(self.order_details)
		return '%s (%s)\n%s\n%s-%s, %s, %s\n%s' %(
				order_detail_dict['delivery_name'], 
				order_detail_dict['delivery_email'],
				order_detail_dict['delivery_address'],
				order_detail_dict['delivery_city'],
				order_detail_dict['delivery_zip'],
				order_detail_dict['delivery_state'],
				order_detail_dict['delivery_country'],
				order_detail_dict['delivery_tel']
			)

	def get_transaction_info(self):
		order_detail_dict = json.loads(self.order_details)
		return 'Transaction Id: %s\nPayment mode: %s\nAmount: %s %s\nStatus: %s (%s)\n' % (
				order_detail_dict['tracking_id'],
				order_detail_dict['payment_mode'],
				order_detail_dict['currency'],
				order_detail_dict['amount'],
				order_detail_dict['order_status'],
				order_detail_dict['status_message'],
			)



