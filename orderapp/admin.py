from django.contrib import admin

from orderapp.models import *

class FramePrefInline(admin.TabularInline):
	model 		=	FramePreferences

class FrameAdmin(admin.ModelAdmin):
	list_display 	= 	('name', 'image')
	inlines 		=	[FramePrefInline]

admin.site.register(Photo)
admin.site.register(Frame, FrameAdmin)
admin.site.register(PrintType)
admin.site.register(Cardboard)