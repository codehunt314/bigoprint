from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, render_to_response
from django.template import RequestContext, loader, Context

from django.forms import ModelForm
from django.forms.models import model_to_dict
import json, os, urllib

from bigoprint import settings
from ccavutil import *
from orderapp.models import *
from bigoprint.ccavenue_credential import *
from customer.models import Feedback
from django.views.decorators.csrf import csrf_exempt
from datetime import timedelta


class PhotoForm(ModelForm):
	class Meta:
		model = Photo
		exclude = ()

class OrderForm(ModelForm):
	class Meta:
		model = PrintOrder
		exclude = ()


def home(request):
	return render_to_response('home.html', context_instance=RequestContext(request))

def try_home(request):
	width 				= 	request.GET.get('width', '36')
	height 				= 	request.GET.get('height', '24')
	printtype_options 	= 	[(pt.name, pt.cost, pt.unit) for pt in PrintType.objects.all()]
	cardboard_options 	=	[(cb.name, cb.colorcode, cb.cbcost, cb.cbcost_unit) for cb in Cardboard.objects.all()]
	frame_options 		=	{}

	for frame in Frame.objects.all():
		frame_options[frame.name] 	=	{
											'image'			: frame.image.name,
											'corner_size' 	: frame.corner_size,
											'pref'			: [{'cost'	: float(frm_pref.cost), 'thickness'	: float(frm_pref.thickness)} for frm_pref in FramePreferences.objects.filter(frame=frame)]
										}

	return render_to_response('customize.html', {
				'width'				: 	width,
				'height'			: 	height,
				'printtype_options'	: 	printtype_options,
				'cardboard_options'	:	cardboard_options,
				'frame_options' 	:	frame_options,
				'frame_options_json':	json.dumps(frame_options),
			}, context_instance=RequestContext(request))


def checkout(request):
	if request.method=='GET':
		return HttpResponse('Wrong reuest type')
	photo 			= 	Photo.objects.create()
	photo_obj_dict	=	model_to_dict(photo)
	cost 			=	request.POST.pop('cost')
	photo_obj_dict.update(request.POST)
	photoform 		= 	PhotoForm(photo_obj_dict, request.FILES, instance=photo)
	if photoform.is_valid():
		ph 			= 	photoform.save(commit=True)
		frame_type 			=	request.POST.get('type')
		frame_width			=	float(request.POST.get('frameWidth'))
		frame_pref, created =	FramePreferences.objects.get_or_create(frame=Frame.objects.get(name=frame_type), thickness=str(frame_width))
		cardboard, created 	=	Cardboard.objects.get_or_create(name=request.POST.get('mat_type'))
		if request.user.id:
			customer 		=	Customer.objects.get(user=request._cached_user)
		else:
			customer 		=	None
		cardboard_thickness =	float(request.POST.get('matWidth'))
		print_width 		=	float(request.POST.get('imageWidth'))
		print_height 		=	float(request.POST.get('imageHeight'))
		final_width 		=	print_width + 2 * (cardboard_thickness + frame_width)
		final_height 		=	print_height + 2 * (cardboard_thickness + frame_width)
		with_glass 			=	request.POST.get('with_glass', 'false') == 'true'
		porder, created 	=	PrintOrder.objects.get_or_create(
									photo 			=	ph,
									print_type 		=	None,
									final_width 	=	str(final_width),
									final_height 	=	str(final_height),
									print_width 	=	str(print_width),
									print_height 	=	str(print_height),
									wall_color_bg 	=	request.POST.get('wall_color'),
									ordered_by		=	customer,
									total_cost 		=	'0.0',
									offered_price 	=	'0.0',
									frame 			=	frame_pref,
									cardboard 		=	cardboard,
									with_glass 		=	True if with_glass else False,
									cardboard_thickness 	=	str(cardboard_thickness)
								)
		total_cost, offered_price 	=	porder.calculate_cost()
		porder.total_cost 			= 	total_cost
		porder.offered_price 		= 	offered_price
		porder.save()
		porder.update_estimated_time()
		order_status, crtd 			= 	OrderStatus.objects.get_or_create(order=porder, status='Order Created')
		return HttpResponseRedirect('/order/%s/' % (porder.id))
	return HttpResponse(json.dumps(photoform.errors))


def _order_detail(current_user, print_order, ready_for_payment=False):
	if current_user:
		customer 		=	Customer.objects.get(user=current_user)
		customer_addrs 	=	[cus_addr.address for cus_addr in CustomerAddress.objects.filter(customer=customer)]
	else:
		customer_addrs 	=	[]
	order_status 		=	OrderStatus.objects.filter(order=print_order).latest('status_time')
	if ready_for_payment:
		order_status, created 	= 	OrderStatus.objects.get_or_create(order=print_order, status='Order Placed')
	try:
		order_confirmed =	OrderStatus.objects.get(order=print_order, status='Order Confirmed')
	except:
		order_confirmed =	False
	confirmed 	=	True if order_confirmed else False

	order_status_index = [o[0] for o in ORDER_STATUS].index(order_status.status)

	return {
				'current_order'	: 	print_order,
				'current_user'	: 	current_user,
				'addresses'		:	customer_addrs,
				'address' 		:	print_order.address.address if print_order.address else None,
				'ready_for_payment': ready_for_payment,
				'order_status' 	:	order_status.status,
				'order_status_index' : order_status_index,
				'confirmed' 	:	confirmed
		}


def order_detail(request, orderid):
	print_order 		=	PrintOrder.objects.get(id=orderid)
	current_user 		=	request._cached_user if request.user.id else None
	if current_user and (not print_order.ordered_by):
		customer 		=	Customer.objects.get(user=current_user)
		print_order.ordered_by 	=	customer
		print_order.save()
	ready_for_payment 	=	request.session.get('addr_saved') and print_order.address is not None
	ret_dict 			=	_order_detail(current_user, print_order, ready_for_payment)
	request.session['addr_saved'] = False
	return render_to_response(
				'order.html',
				ret_dict,
				context_instance	= 	RequestContext(request)
			)


def fetch_resources(uri, rel):
	path 	= 	os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ""))
	return path


def order_print(request, orderid):
	if not request.user.id:
		return HttpResponseRedirect('/customer/login/?redirect_url=%s'%(request.path))
	import ho.pisa as pisa
	import cStringIO as StringIO
	print_order 		=	PrintOrder.objects.get(id=orderid)
	order_status 		=	OrderStatus.objects.filter(order=print_order).latest('status_time')
	current_user 		=	request._cached_user if request.user.id else None
	if current_user:
		customer 		=	Customer.objects.get(user=current_user)
		customer_addrs 	=	[cus_addr.address for cus_addr in CustomerAddress.objects.filter(customer=customer)]
	else:
		customer_addrs 	=	[]

	html 	=	loader.render_to_string(
					'order_print.html',
					{
						'pagesize' 		: 	'A4',
						'current_order'	: 	print_order,
						'current_user'	: 	current_user,
						'addresses'		:	customer_addrs,
						'address' 		:	print_order.address.address if print_order.address else None,
						'order_status' 	:	order_status.status
					},
					context_instance=RequestContext(request)
				)
	result 	= 	StringIO.StringIO()
	pdf 	= 	pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), dest=result, link_callback=fetch_resources )
	if not pdf.err:
		return HttpResponse(result.getvalue(), content_type='application/pdf')
	return HttpResponse('someone ate your pdf! %s' % cgi.escape(html))


def order_edit(request, orderid):
	if not request.user.id:
		return HttpResponseRedirect('/customer/login/?redirect_url=%s'%(request.path))
	current_user 		=	request._cached_user if request.user.id else None
	print_order 		=	PrintOrder.objects.get(id=orderid)
	if request.method=='POST':
		photo 			= 	print_order.photo
		photo_obj_dict	=	model_to_dict(photo)
		cost 			=	request.POST.pop('cost')
		photo_obj_dict.update(request.POST)
		photoform 		= 	PhotoForm(photo_obj_dict, request.FILES, instance=photo)
		if photoform.is_valid():
			ph 								= 	photoform.save(commit=True)
			print_order.photo 				=	ph
			frame_type 						=	request.POST.get('type')
			frame_width						=	float(request.POST.get('frameWidth'))
			frame_pref, created 			=	FramePreferences.objects.get_or_create(frame=Frame.objects.get(name=frame_type), thickness=str(frame_width))
			cardboard, created 				=	Cardboard.objects.get_or_create(name=request.POST.get('mat_type'))

			print_order.cardboard_thickness =	float(request.POST.get('matWidth', print_order.cardboard_thickness))
			print_order.print_width 		=	float(request.POST.get('imageWidth', print_order.print_width))
			print_order.print_height 		=	float(request.POST.get('imageHeight', print_order.print_height))
			print_order.final_width 		=	print_order.print_width + 2 * (print_order.cardboard_thickness + frame_width)
			print_order.final_height 		=	print_order.print_height + 2 * (print_order.cardboard_thickness + frame_width)
			print_order.wall_color_bg 		=	request.POST.get('wall_color', print_order.wall_color_bg)
			print_order.with_glass 			=	request.POST.get('with_glass', 'false') == 'true'

			print_order.frame 				=	frame_pref
			print_order.cardboard 			=	cardboard

			total_cost, offered_price 		=	print_order.calculate_cost()
			print_order.total_cost 			= 	total_cost
			print_order.offered_price 		= 	offered_price

			print_order.save()
			print_order.update_estimated_time()
			return HttpResponseRedirect('/order/%s/'%(orderid))
	else:
		return render_to_response('customize.html', {
			'order_object' : json.dumps({
				'id'			: 	print_order.id,
				'imageWidth'	:	float(print_order.print_width),
				'imageHeight'	:	float(print_order.print_height),
				'matWidth'		:	float(print_order.cardboard_thickness),
				'frameWidth'	:	float(print_order.frame.thickness),
				'type'			:	print_order.frame.frame.name,
				'mat_type'		:	print_order.cardboard.name,
				'wall_color'	:	print_order.wall_color_bg,
				'with_glass'	:	print_order.with_glass,
				'image_path'	:	print_order.photo.imagefile.url
			})
		}, context_instance=RequestContext(request))

def orders_list(request):
	if request.user.id:
		current_user 	=	Customer.objects.get(user=request._cached_user)
		user_orders 	=	[]
		for uo in PrintOrder.objects.filter(ordered_by=current_user):
			order_status, current_status = uo.order_status()
			uo.__setattr__('current_status', current_status)
			user_orders.append(uo)
		return render_to_response('orders_list.html', {'user_orders': user_orders}, context_instance=RequestContext(request))
	return HttpResponseRedirect('/customer/login/?redirect_url=%s'%(request.path))


@csrf_exempt
def order_confirm(request, orderid):
	if not request.user.id:
		return HttpResponseRedirect('/customer/login/?redirect_url=/order/%s/'%(orderid))
	current_user 		=	request.user if request.user.id else None
	print_order 		=	PrintOrder.objects.get(id=orderid)

	encResp = "85fe16f3a62e38bf76235a7ea4e29cfa3a8420883e6a0f7c3f25331db3822874029093f8f450779eda4a10c88da02cb3655cb95cdbc153012ccada3935a262fba828482c48904aa72f82f63a48818d72b895af912b98fa39bcf6d69fdbf8d6533b33618f336ae6e3380383f1b6deb5030954cecd334d000a34ebe42d33c1d2f65b21192860f159ff21e0d3b0aa3ae1119bbcaba7e1531a1dbd1f111fdfd9f31cf445379fbb53e48308583dcc432757b5f0223b5a4b5b8d7702e13557f6a1fb159d0e4f4d1a3b4c8819bab076ff6614b2d0c7f99fe4f354c076af1c114d4acff75c9b22070f23d94388a1fe6136e4104c17ca96d380b2f1765969461b10e71c28c95337931752cad982ec436c6abcea1a7664c45046d1fba6e7fbdaffd1142dc130c03669712cad9b064de799c9494a506dedc745bce24251210adf32b28141a19f2627142964eb1d94928f7742372fbd4b7ebaa42e0bd37767c7b543b493fee5774604ca33e9c6333cdb1a4e9a718fa3c8fc653962ec0dff7a4ae762e68421c117217af766be5cfd0618d19ebb8ac7c02c815728b2cf8210b3e3f8f06f6d343b1cea6e7a4f4557e33d3dd7354ed842f65d2fe9aa40643c5dfbb51d8826f4edd9d97e2fa9a21785c7af10e63c90ff84a91d2c3edec438f05c16bddbc672e90e846c2d5ac869571fb29b6ea61c6e81669a34ab41ffb74612aed12f4f5bc2263177ccd18386d335491a402193f47c9218d46321224e2e8931ed56a2dbdca3ce7321ac3f88cee7748bf454f782323ff07145bdc1470299ca19c330ffbfad9e57eb62137f9fbeb78fb7ffcd644409157df069518d9f94075ea7b1804c514c38180f1bacf105d023c82828ef3ec0072394af772d33d7e019d5d589026cedb7447f7aba141b8761611f56ee48b0a92ff3830b1debf9d9a9a879991f8ef0b9b5388bd131b94b62e60ede9e255eb267c945e8895cf694be889bd8eba3374ce8cbd1ab1248af6b7f8f7959645c128ded63cdafa02734311eb82a9decd6784562cdda64c2d00d55048c3702a045a78433a0239601ffcc15ecce979c603438118be9021b6cb8b2f10e7b5f3c4c805e2d826a5857df52789055a225924859aa2e9077b9ff27cc8570d4893716e20c3daa983f0187f54005c6767219e0d8e316045bbbb61c0c2d74cdc07d8a27ac46675562dccdde9c80e02d7323ad76e7662b06d4e5b7fbfd8f3b428efa30ef785bc00d71d78ebe44a9757d361785a8196284a25bd7756d55b985e58b2367fe1651a1e7d00fc8a82700f154c1006a4c719b6204b9b6654cc70f"
	response_enc 	= 	request.POST.get('encResp', encResp)
	# response_enc 	= 	request.POST.get('encResp', None)
	order_no 		= 	request.POST.get('orderNo', orderid)
	if order_no==orderid and response_enc:
		data 		= 	decrypt(response_enc, working_key)
		data_dict 	= 	dict([tuple(i.split('=')) for i in data.split('&')])
		if data_dict.get('order_status', '')=='Success':
			order_status, crtd 	= 	OrderStatus.objects.get_or_create(order=print_order, status='Order Confirmed')
		else:
			order_status, crtd 	= 	OrderStatus.objects.get_or_create(order=print_order, status=data_dict.get('order_status', 'Transaction Failed'))
		order_payment, crtd 	=	OrderPGResponse.objects.get_or_create(order_status=order_status, order_details=json.dumps(data_dict))
		print_order.save()
		print_order.update_estimated_time()
		return HttpResponseRedirect('/order/%s/'%(print_order.id))
	return HttpResponse('Some inconsistency in data, orderid is not matching')


@csrf_exempt
def cancel_payment(request, orderid):
	if not request.user.id:
		return HttpResponseRedirect('/customer/login/?redirect_url=/order/%s/'%(orderid))
	current_user 		=	request.user if request.user.id else None
	print_order 		=	PrintOrder.objects.get(id=orderid)
	response_enc 		= 	request.POST.get('encResp', None)
	order_no 			= 	request.POST.get('orderNo', orderid)
	if order_no==orderid and response_enc:
		data 			= 	decrypt(response_enc, working_key)
		data_dict 		= 	dict([tuple(i.split('=')) for i in data.split('&')])
		order_status, crtd 		= 	OrderStatus.objects.get_or_create(order=print_order, status=data_dict.get('order_status', 'Transaction Failed'))
		order_payment, crtd 	=	OrderPGResponse.objects.get_or_create(order_status=order_status, order_details=json.dumps(data_dict))
	return HttpResponseRedirect('/order/%s/'%(print_order.id))



def makepayment(request, orderid):
	if not request.user.id:
		return HttpResponseRedirect('/customer/login/?redirect_url=/order/%s/'%(orderid))
	print_order 		=	PrintOrder.objects.get(id=orderid)
	if request.method=='POST':
		p_merchant_id 		= str(merchant_id)
		p_order_id 			= orderid
		p_currency 			= 'INR'
		p_amount 			= str(print_order.offered_price)
		p_redirect_url 		= 'https://%s/order/%s/confirm/'%(request.get_host(), print_order.id)
		p_cancel_url 		= 'https://%s/order/%s/cancel_payment/'%(request.get_host(), print_order.id)
		p_language 			= 'en'
		p_billing_name 		= print_order.address.address.fullname
		p_billing_address 	= print_order.address.address.address
		p_billing_city 		= print_order.address.address.city
		p_billing_state 	= print_order.address.address.city
		p_billing_zip 		= print_order.address.address.pincode
		p_billing_country 	= print_order.address.address.country
		p_billing_tel 		= print_order.address.address.contact_no
		p_billing_email 	= print_order.address.customer.user.email
		p_delivery_name 	= p_billing_name
		p_delivery_address 	= p_billing_address
		p_delivery_city 	= p_billing_city
		p_delivery_state 	= p_billing_state
		p_delivery_zip 		= p_billing_zip
		p_delivery_country 	= p_billing_country
		p_delivery_tel 		= p_billing_tel

		encoded_param	=	'merchant_id='+p_merchant_id+'&'+'order_id='+p_order_id + '&' + "currency=" + p_currency + '&' + \
		'amount=' + p_amount+'&'+'redirect_url='+p_redirect_url+'&'+'cancel_url='+p_cancel_url+'&'+\
		'language='+p_language+'&'+'billing_name='+p_billing_name+'&'+'billing_address='+p_billing_address+'&'+\
		'billing_city='+p_billing_city+'&'+'billing_state='+p_billing_state+'&'+'billing_zip='+p_billing_zip+'&'+\
		'billing_country='+p_billing_country+'&'+'billing_tel='+p_billing_tel+'&'+'billing_email='+p_billing_email+'&'+\
		'delivery_name='+p_delivery_name+'&'+'delivery_address='+p_delivery_address+'&'+'delivery_city='+p_delivery_city+\
		'&'+'delivery_state='+p_delivery_state+'&'+'delivery_zip='+p_delivery_zip+'&'+'delivery_country='+\
		p_delivery_country+'&'+'delivery_tel='+p_delivery_tel+'&'

		encryption = encrypt(encoded_param, working_key)
		return render_to_response('ccavenue_payment.html', {'encryption': encryption, 'access_code': access_code})


