print_cost_data = {
	'self_adhesive_sticker': {
		'lavel': 'Self Adhesive Filter',
		'cost' : 60,
		'unit' : 'sq-ft'
	},
	'photo_paper': {
		'lavel': 'Photo Paper',
		'cost' : 60,
		'unit' : 'sq-ft'
	},
	'eco_vinyl': {
		'lavel': 'Eco Vinyl',
		'cost' : 40,
		'unit' : 'sq-ft'
	}
}
def print_cost(w, h, print_type='photo_paper', quality=300):
	width_inch 		= 	float(w)/quality
	height_inch 	= 	float(h)/quality
	area_sqft 		=	(width_inch/12) * (height_inch/12)
	return width_inch, height_inch, area_sqft * print_cost_data[print_type]['cost']

def framing_cost(w, h, frame_cost=12, with_border=True):
	total_peripheri = 	2*(w + h)
	if with_border:
		return total_peripheri * 16
	else:
		cardboard_cost 	=	50 * (float(w)/12) * (float(h)/12)
		frame_cost 		=	total_peripheri * 12
		return frame_cost + cardboard_cost

def total_cost(w, h, print_type='photo_paper', quality=300, with_border=True, margin=1.25):
	w_inch, h_inch, cost1 = print_cost(w, h, print_type, quality)
	cost2 	=	framing_cost(w_inch, h_inch, with_border=with_border)
	delivery_cost = 100
	return w_inch, h_inch, margin * (cost1 + cost2 + delivery_cost)

