# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from decimal import Decimal
import orderapp.models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cardboard',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, blank=True)),
                ('colorcode', models.CharField(default=b'#fafafa', max_length=255)),
                ('image', models.ImageField(null=True, upload_to=b'uploaded/cardboard/', blank=True)),
                ('cbcost', models.DecimalField(default=Decimal('50.00'), max_digits=5, decimal_places=2)),
                ('cbcost_unit', models.CharField(default=b'sqft', max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Frame',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, null=True, blank=True)),
                ('colorcode', models.CharField(max_length=255, null=True, blank=True)),
                ('image', models.ImageField(null=True, upload_to=b'uploaded/frame', blank=True)),
                ('corner_size', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FramePreferences',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('thickness', models.DecimalField(default=Decimal('0.00'), max_digits=3, decimal_places=2)),
                ('cost', models.DecimalField(default=Decimal('10.00'), max_digits=5, decimal_places=2)),
                ('unit', models.CharField(default=b'inch', max_length=255)),
                ('frame', models.ForeignKey(to='orderapp.Frame')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrderPGResponse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order_details', models.TextField(null=True, blank=True)),
                ('status_time', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrderStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status_time', models.DateTimeField(auto_now_add=True)),
                ('status', models.CharField(default=b'Order Created', max_length=255, choices=[(b'Order Created', b'Order Created'), (b'Order Placed', b'Order Placed'), (b'Payment Done', b'Payment Done'), (b'Order Confirmed', b'Order Confirmed'), (b'Aborted', b'Aborted'), (b'Failure', b'Failure'), (b'Invalid', b'Invalid'), (b'In Processing', b'In Processing'), (b'In Transit', b'In Transit'), (b'Delivered', b'Delivered'), (b'Replacement', b'Replacement'), (b'Other', b'Other')])),
                ('status_info', models.TextField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('imagefile', models.ImageField(height_field=b'y_pixel', width_field=b'x_pixel', upload_to=orderapp.models.uploaddir)),
                ('upload_time', models.DateTimeField(auto_now_add=True)),
                ('x_pixel', models.PositiveIntegerField(null=True, blank=True)),
                ('y_pixel', models.PositiveIntegerField(null=True, blank=True)),
                ('cost', models.DecimalField(default=Decimal('0.00'), max_digits=9, decimal_places=2)),
                ('uploaded_by', models.ForeignKey(blank=True, to='customer.Customer', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PrintOrder',
            fields=[
                ('id', orderapp.models.ArbitIDField(max_length=50, serialize=False, editable=False, primary_key=True)),
                ('print_width', models.DecimalField(default=Decimal('0.00'), max_digits=4, decimal_places=2)),
                ('print_height', models.DecimalField(default=Decimal('0.00'), max_digits=4, decimal_places=2)),
                ('final_width', models.DecimalField(default=Decimal('0.00'), max_digits=4, decimal_places=2)),
                ('final_height', models.DecimalField(default=Decimal('0.00'), max_digits=4, decimal_places=2)),
                ('cardboard_thickness', models.DecimalField(default=Decimal('0.00'), max_digits=3, decimal_places=2)),
                ('with_glass', models.BooleanField(default=True)),
                ('wall_color_bg', models.CharField(default=b'#aaa', max_length=255)),
                ('ordered_time', models.DateTimeField(auto_now=True, auto_now_add=True)),
                ('total_cost', models.DecimalField(default=Decimal('0.00'), max_digits=9, decimal_places=2)),
                ('offered_price', models.DecimalField(default=Decimal('0.00'), max_digits=9, decimal_places=2)),
                ('estimated_date', models.DateField(default=b'2015-06-30', auto_now_add=True)),
                ('address', models.ForeignKey(blank=True, to='customer.CustomerAddress', null=True)),
                ('cardboard', models.ForeignKey(blank=True, to='orderapp.Cardboard', null=True)),
                ('frame', models.ForeignKey(blank=True, to='orderapp.FramePreferences', null=True)),
                ('ordered_by', models.ForeignKey(blank=True, to='customer.Customer', null=True)),
                ('photo', models.ForeignKey(to='orderapp.Photo')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PrintType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('cost', models.DecimalField(default=Decimal('10.00'), max_digits=6, decimal_places=2)),
                ('unit', models.CharField(default=b'sqft', max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='printorder',
            name='print_type',
            field=models.ForeignKey(blank=True, to='orderapp.PrintType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orderstatus',
            name='order',
            field=models.ForeignKey(to='orderapp.PrintOrder'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orderpgresponse',
            name='order_status',
            field=models.ForeignKey(to='orderapp.OrderStatus'),
            preserve_default=True,
        ),
    ]
