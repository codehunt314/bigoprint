from __future__ import with_statement
from fabric.api import *                  #local, run, env, cd, hosts

demo_server_repo = ""
virtual_env_path = ""
demo_fcgi_pid_dir = ""

@hosts()
def deploy_framd():
    with cd(demo_server_repo):
        run("git pull origin master")
    with settings(warn_only=True):
        print "Stopping FCGI Server...."
        result = run("cat %s/fcgi_process.file |xargs kill -9" % (demo_fcgi_pid_dir))
        if result.failed: print "No FCGI running"
    with cd(demo_server_repo):
        print "Starting FCGI Server...."
        #run("source %s && python manage.py syncdb --settings=bigoprint.settings && python manage.py runfcgi --settings=bigoprint.settings pidfile=%s/fcgi_process.file host=127.0.0.1 port=8888" % (virtual_env_path, demo_fcgi_pid_dir), shell=True)
        run("source %s && python manage.py runfcgi --settings=bigoprint.settings pidfile=%s/fcgi_process.file host=127.0.0.1 port=8888" % (virtual_env_path, demo_fcgi_pid_dir), shell=True)
        # run("source %s && python manage.py collectstatic --settings=bigoprint.settings && python manage.py runfcgi --settings=bigoprint.settings pidfile=%s/fcgi_process.file host=127.0.0.1 port=8888" % (virtual_env_path, demo_fcgi_pid_dir), shell=True)
        print "Restarting nginx Server...."
        run("/etc/init.d/nginx restart")


@hosts()
def stop_framd():
    with cd(demo_server_repo):
        with settings(warn_only=True):
            print "Stopping FCGI Server...."
            result = run("cat %s/fcgi_process.file |xargs kill -9" % (demo_fcgi_pid_dir))
            if result.failed: print "No FCGI running"


@hosts()
def deploy_framd_migrate():
    with cd(demo_server_repo):
    	run("git pull origin master")
        run("source %s && python manage.py makemigrations && python manage.py migrate" % (virtual_env_path))


