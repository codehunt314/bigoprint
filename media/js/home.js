/*global $:false*/
'use strict';

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

var getBasePrice = function(width, height) {
	var borderPrice = 9, printPrice = 60;
	var border = 2 * (width+1) + 2 * (height + 1);
	var area = (width / 12) * (height / 12);
	var price = border * borderPrice + area * printPrice;
	return numberWithCommas(Math.ceil(price * 1.25 + 150)) + '.00';
};

$(function() {
	var updatePrice = function(){
		var price = getBasePrice(parseFloat($('#input-width').val(), 10), parseFloat($('#input-height').val(), 10));
		$('.price-span').html(price);
	};

	$('.get-started-btn').click(function(e) {
		e.preventDefault();
		$('body').addClass('modal-open');
	});

	$('.modal-fade-screen, .modal-close').on('click', function() {
		$('body').removeClass('modal-open');
	});

	$('.modal-inner').on('click', function(e) {
		e.stopPropagation();
	});

	$('.input-dimension').keyup(function(){
		updatePrice();
	});

	$('.hamburger, .overlay').click(function(){
		$('body').toggleClass('sidebar-open');
	});
	updatePrice();
});