/*global define:false */
define(function(require) {
	'use strict';

	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone');

	var OrderView = Backbone.View.extend({
		events: {
			'change .signin-radio': 'changedSignInOption',
			'click .js-edit-address': 'editAddressModal',
			'click .js-delete-address': 'deleteAddressConfirm',
			'click .edit-address-btn': 'saveAddress'
		},
		changedSignInOption: function() {
			if (this.$('#radio-new-user:checked').length > 0) {
				this.$('#txt-password').attr('disabled', 'disabled');
			} else {
				this.$('#txt-password').removeAttr('disabled');
			}
		},
		deleteAddressConfirm: function(e) {
			if (window.confirm('Do you really want to delete this address?')) {
				var $form = $(e.target).prev('form');
				$.post($form.attr('action'), $form.serialize())
				 .done(function(response){
				 	$(e.target).parents('.address-block').remove();
				 });
			}
		},
		editAddressModal: function(e) {
			var $form = $(e.target).parents('.address-block').find('.edit-address-form');
			var $div = $('#modal-address-div').empty();
			$form = $form.clone();
			$form.data('id', $(e.target).data('id'));
			$form.removeClass('hide').appendTo($div);
			$form.submit(_.bind(this.saveAddress, this));
			$('body').addClass('modal-open');
		},
		saveAddress: function(e){
			e.preventDefault();
			e.stopPropagation();
			var $form = $(e.target),
				id = $form.data('id');
			$.post($form.attr('action'), $form.serialize())
			 .done(function(response){
			 	$('#address-block-'+id).replaceWith(response);
			 	$('body').removeClass('modal-open');
			 });
		}
	});

	$(document).ready(function() {
		var $el = $('.order-detail');
		if ($el.length === 1) {
			new OrderView({
				el: $el
			});
		}

		$('.modal-fade-screen, .modal-close').on('click', function() {
			$('body').removeClass('modal-open');
		});

		$('.modal-inner').on('click', function(e) {
			e.stopPropagation();
		});
	});

});