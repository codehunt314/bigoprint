/*global define:false */
define(function (require) {
	'use strict';

	var $ = require('jquery');
	$(document).ready(function(){
		$('h1.logo').click(function(e){
			e.stopPropagation();
			$('body').toggleClass('show-sidenav');
			if($('body').hasClass('show-sidenav')){
				$('.wrapper').on('click', function(){
					console.log('hidden');
					$('body').toggleClass('show-sidenav');
					$('.wrapper').off('click');
				});
			}
		});
	});
});