// Set the require.js configuration for your application.
require.config({

  // Initialize the application with the main application file.
  deps: ['entry'],
  // baseUrl: '/js/app/',
  // appDir: ".",
  paths: {
    // 'main' : '../main-built.min',

    // JavaScript folders.
    'templates'              : 'templates',

    // Libraries
    'jquery'                 : '../../bower_components/jquery/dist/jquery.min',
    'underscore'             : '../../bower_components/underscore/underscore-min',
    'backbone'               : '../../bower_components/backbone/backbone',
    'backbone-package'       : '../vendor/backbone-packages',
    'backbone-epoxy'         : '../../bower_components/backbone.epoxy/backbone.epoxy',


    // Plugins
    'jrange'                 : '../plugins/jRange/jquery.range',
    'spectrum'               : '../../bower_components/spectrum/spectrum',
    'text'                   : '../vendor/text',
    'domready'               : '../vendor/domReady',
    // 'async'                  : '../plugins/async',

  },

  shim: {

    jquery: {
      exports: 'jQuery'
    },

    underscore: {
      exports: '_'
    },

    backbone: {
      deps: [ 'underscore', 'jquery' ],
      exports: 'Backbone'
    },

    'backbone.epoxy': {
      deps : 'backbone'
    },

    'jrange':{
      deps: ['jquery']
    }
  }
});
