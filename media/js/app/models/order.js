/*global define:false */
define(function(require) {
	'use strict';
	var _ = require('underscore'),
		Backbone = require('backbone');

	var Order = Backbone.Model.extend({
		defaults: {
			ppi: 300,
			unit: 'inch'
		},
		initialize: function(){
			this.on('change:with_glass change:imageWidth change:imageHeight change:matWidth change:frameWidth', _.bind(this.updateCost, this));
			this.updateCost();
		},
		updateCost: function(){
			this.set('cost', this.getCost());
		},
		getInInch: function(x) {
			switch (this.get('unit')) {
				case "feet":
					return x * 12;
				case "inch":
					return x;
				case "cm":
					return x * 0.3937;
				case "meter":
					return x * 39.37;
			}
		},
		getFrameObject: function(maxWidth, maxHeight) {
			var frameSize = this.getActualFrameSize();
			var sizeInInch = {
				x: this.getInInch(frameSize.x),
				y: this.getInInch(frameSize.y)
			};
			var pixelRatio = Math.min(maxHeight / sizeInInch.y, maxWidth / sizeInInch.x);
			var frame = {
				x: sizeInInch.x * pixelRatio,
				y: sizeInInch.y * pixelRatio
			};
			frame.matColorValue = this.getMatColorValue();
			frame.matWidth = Math.round(this.get('matWidth') * pixelRatio);
			frame.optimizedMatWidth = frame.matWidth;
			frame = _.extend({}, frame, this.getFrameColors());
			frame.optimizedFrameWidth = Math.round(this.get('frameWidth') * pixelRatio);
			frame.cost = this.getCost();
			return frame;
		},
		getActualFrameSize: function() {
			return {
				x: this.get('imageWidth') + (2 * (this.get('frameWidth') + this.get('matWidth'))),
				y: this.get('imageHeight') + (2 * (this.get('frameWidth') + this.get('matWidth')))
			};
		},
		getCost: function() {
			var totalCost = 0.0,
				printRate = 60.0,
				matRate = 0.0,
				fullSize = this.getActualFrameSize(),
				printMatCost = this.get('imageWidth') / 12.0 * this.get('imageHeight') / 12.0 * (printRate + matRate),
				frameRate, framingCost, glassCost = 0;

			if (this.get('matWidth') > 0) {
				frameRate = 7 + ((this.get('frameWidth') - 0.5) / 0.25) + 3.00; //mounting + glass + framing
			} else {
				frameRate = 7 + ((this.get('frameWidth') - 0.5) / 0.25) + 2.00; //glass + framing
			}
			framingCost = 2 * (fullSize.x + fullSize.y) * frameRate;
			totalCost = printMatCost + framingCost;
			if( this.has('with_glass') && !this.get('with_glass') ){
				glassCost = 0.25 * (fullSize.x) * (fullSize.y);
			}
			totalCost = totalCost - glassCost;
			totalCost = Math.floor(totalCost);
			return Math.floor(totalCost * 1.25 + 150);
		},
		getMatColorValue: function() {
			switch (this.get('mat_type')) {
				case "white":
					return "hsla(0, 0%, 100%, 1)";
				case "blue":
					return "hsla(187, 100%, 94%, 1)";
				case "black":
					return "hsla(0, 0%, 30%, 1)";
			}
		},
		getFrameColors: function() {
			switch (this.get('type')) {
				case "black":
					return {
						hue: 0,
						saturation: 0,
						topLightness: 16,
						sideLightness: 13
					};
				case "white":
					return {
						hue: 0,
						saturation: 0,
						topLightness: 96,
						sideLightness: 93
					};
				case "brown":
					return {
						hue: 18,
						saturation: 62,
						topLightness: 27,
						sideLightness: 23
					};
			}
		}
	});
	return Order;
});