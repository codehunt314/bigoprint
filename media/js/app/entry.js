/*global define:false, ORDER_OBJECT */
define(function (require) {
	'use strict';

	var $ = require('jquery');

	$(document).ready(function(){
		$('h1.logo').click(function(e){
			e.stopPropagation();
			$('body').toggleClass('show-sidenav');
			if($('body').hasClass('show-sidenav')){
				$('.wrapper').on('click', function(){
					console.log('hidden');
					$('body').toggleClass('show-sidenav');
					$('.wrapper').off('click');
				});
			}
		});
	});

	var OrderModel = require('models/order');
	var ControlView = require('views/control-view');
	var PreviewView = require('views/preview-view');

	var orderModel;
	if(typeof(ORDER_OBJECT) != 'undefined'){
		orderModel = new OrderModel(JSON.parse(ORDER_OBJECT));
	}else{
		orderModel = new OrderModel({
				imageWidth: WIDTH,
				imageHeight: HEIGHT,
				matWidth: 0.00,
				frameWidth : 0.50,
				type: 'black',
				mat_type: 'white',
				wall_color: '#ffffff',
				with_glass: true
			});
	}
	var controlView = new ControlView({
		el : $('.controls-container'),
		model : orderModel
	}).render();
	var previewView = new PreviewView({
		model : orderModel,
		el : $('.wall-container'),
	}).render();

});