/*globals define:false*/
define(function (require) {
	'use strict';

	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone-package');

	require('jrange');
	require('spectrum');

	var ControlView = Backbone.Epoxy.View.extend({
		events: {
			'click .frame-link' : 'selectFrameType',
			'click .mat-link' : 'selectMatType',
			'click .collapse-link': 'toggleControls',
			'click .buy-now' : 'buyNow',
			'click .link-orientation-lock' : 'toggleOrientationLock',
			'keydown .input-dimension' : 'dimensionKeyPress'
		},
		bindings: {
		    "input#image-height": "value:decimal(imageHeight),events:['keyup']",
		    "input#image-width": "value:decimal(imageWidth),events:['keyup']",
		    "input.mat-width" : "value:decimal(matWidth),events:['change']",
		    "input.frame-width": "value:decimal(frameWidth),events:['change']",
		    "select#unit" : "value:unit,events:['change']",
		    "input#input-with_glass" : "checked:with_glass"
		},
		initialize: function(){
			this.listenTo(this.model, 'change:imageHeight', this.changeImageHeight);
			this.listenTo(this.model, 'change:imageWidth', this.changeImageWidth);
			this.listenTo(this.model, 'change:matWidth', this.changeMatWidth);
			this.listenTo(this.model, 'lock-orientation', this.lockOrientation);
			this.listenTo(this.model, 'change:cost', this.updateCost);
		},
		render: function(){
			var maxWidth = Math.min($(window).width() - 40, 340);
			this.$('.frame-width').jRange({
			    from: 0.50,
			    to: 2.00,
			    step: 0.25,
			    scale: ["0.50","0.75","1.00","1.25","1.50","1.75","2.00"],
			    format: '%s',
			    width: maxWidth,
			    showLabels: true,
			    snap: true
			});
			this.$('.mat-width').jRange({
			    from: 0.0,
			    to: 2.00,
			    step: 0.25,
			    scale: ["0.00","0.25","0.50","0.75","1.00","1.25","1.50","1.75","2.00"],
			    format: '%s',
			    width: maxWidth,
			    showLabels: true,
			    snap: true
			});
			this.updateCost();
			return this;
		},
		changeMatWidth: function(){
			if(this.model.get('matWidth') > 0.0){
				this.model.set('with_glass', true);
				this.$('#input-with_glass').attr('disabled', 'disabled');
			}else{
				this.$('#input-with_glass').removeAttr('disabled');
			}
		},
		updateCost: function(){
			this.$('#id-final-price').html(this.model.get('cost'));
		},
		dimensionKeyPress: function(e){
			var diff = 0;
			switch(e.which){
				case 38:
					diff = 1; break;
				case 40:
					diff = -1; break;
			}
			if(diff !== 0){
				e.preventDefault();
				var $ele = $(e.target),
					value = Number($ele.val()) + diff;
				if(value){
					$ele.val(value).trigger('keyup');
				}
			}

		},
		changeImageHeight: function(){
			if(this.orientation){
				var newWidth = Math.round(this.model.get('imageHeight') * this.orientation);
				this.model.set('imageWidth', newWidth, {silent: true});
				this.$('input#image-width').val(newWidth);
				this.model.trigger('change');
				this.model.updateCost();
			}
			this.validatePPI();
		},
		changeImageWidth: function(){
			if(this.orientation){
				var newHeight = Math.round(this.model.get('imageWidth') / this.orientation);
				this.model.set('imageHeight', newHeight, {silent: true});
				this.$('input#image-height').val(newHeight);
				this.model.trigger('change');
				this.model.updateCost();
			}
			this.validatePPI();
		},
		validatePPI: function(){
			if(this.model.has('imageDimensionWidth')){
				var minPPI = 100,
					maxWidth = Math.round(this.model.get('imageDimensionWidth') / minPPI),
					maxHeight = Math.round(this.model.get('imageDimensionHeight') / minPPI);
				if(this.model.get('imageWidth') > maxWidth || this.model.get('imageHeight') > maxHeight){
					this.$('.size-warning').removeClass('hide');
				}else{
					this.$('.size-warning').addClass('hide');
				}
			}
		},
		selectFrameType: function(e){
			e.stopPropagation();
			e.preventDefault();
			var ele = $(e.currentTarget);
			ele.siblings().removeClass('selected');
			ele.addClass('selected');
			this.model.set('type', ele.data('type'));
		},
		selectMatType: function(e){
			e.stopPropagation();
			e.preventDefault();
			var ele = $(e.currentTarget);
			ele.siblings().removeClass('selected');
			ele.addClass('selected');
			this.model.set('mat_type', ele.data('type'));
		},
		toggleControls: function(){
			this.$('.collapse-link').toggleClass('open');
			this.$el.toggleClass('open');
			$('.wall-container').toggleClass('open');
		},
		buyNow: function(){
			var $btn = $('.buy-now');
			if(!$('.frame').hasClass('has-image')){
				alert('Please select a file to print');
				return;
			}
			var data = this.model.toJSON(),
				$form = $('#form-order');
			if(!this.model.isNew()){
				$form.attr('action', '/order/'+this.model.id+'/edit/');
			}
			_.each(_.keys(data), function(key){
				$('<input />', {
					type: 'hidden',
					name: key,
					value: data[key]
				}).appendTo($form);
			});
			$('.spinner').css({
				'width' : $btn.outerWidth(),
				'height': $btn.outerHeight(),
				'top' : $btn.position().top,
				'left' : $btn.position().left
			}).addClass('visible');
			$form.submit();
		},
		lockOrientation: function(){
			this.$('.link-orientation-lock').addClass('selected');
			this.orientation = this.model.get('imageWidth') / this.model.get('imageHeight');
		},
		toggleOrientationLock: function(e){
			e.stopPropagation();
			e.preventDefault();
			this.$('.link-orientation-lock').toggleClass('selected');
			if(this.$('.link-orientation-lock').hasClass('selected')){
				this.orientation = this.model.get('imageWidth') / this.model.get('imageHeight');
			}else{
				delete this.orientation;
			}
		}
	});

	return ControlView;
});