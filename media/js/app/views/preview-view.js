/*globals define:false, $:false*/
define(function (require) {
	'use strict';

	var _ = require('underscore'),
		Backbone = require('backbone-package');

	var PreviewView = Backbone.Epoxy.View.extend({
		events: {
			'click .frame:not(.has-image)' : 'selectImage',
			'click .change-image' : 'selectImage',
			'change .file-input': 'fileSelected'
		},
		initialize: function() {
			this.listenTo(this.model, 'change', this.onChange);
		    this.$('#colorpicker').spectrum({
		        color: '#f00',
		        move: _.bind(function(color) {
		            this.changeWallColor(color.toHexString());
		        }, this)
		    });
		    var lazyLayout = _.debounce(_.bind(this.render, this), 300);
		    $(window).resize(lazyLayout);
		},
		onChange: function(model){
			// console.log(model.toJSON());
			this.render();
		},
		getMaxSize: function(){
			var windowWidth = $(window).width(),
				windowHeight = $(window).height(),
				isPotrait = windowHeight > windowWidth, MAX_HEIGHT, MAX_WIDTH;

			if(isPotrait){
				if( windowWidth < 768){
					MAX_HEIGHT = windowHeight;
					MAX_WIDTH = windowWidth;
				}else{
					MAX_HEIGHT = windowHeight / 2;
					MAX_WIDTH = windowWidth / 2;
				}
				return [
					Math.min(windowWidth  - 40, MAX_WIDTH),
					Math.min(windowHeight - 46 - 20 - 75 - 38 - 60, MAX_HEIGHT),
				];
			}else{
				if(windowWidth < 768){
					MAX_HEIGHT = windowHeight;
					MAX_WIDTH = windowWidth;
					return [
						Math.min(windowWidth  - 40, MAX_WIDTH),
						Math.min(windowHeight - 46 - 88, MAX_HEIGHT),
					];
				}else{
					MAX_HEIGHT = windowHeight / 2;
					MAX_WIDTH = windowWidth / 2;
					return [
						Math.min(windowWidth  - 40, MAX_WIDTH),
						Math.min(windowHeight - 46 - 20 - 75 - 38 - 60, MAX_HEIGHT),
					];
				}
			}
		},
		render: function(){
			var maxSize = this.getMaxSize();
			var frame = this.model.getFrameObject.apply(this.model, maxSize);
			this.$('.frame-box').css({
				height: frame.y
			});
			this.$('.frame-container,.frame-shadow').css({
				width  : frame.x,
				height : frame.y,
			});
			this.$('.frame').css({
				width  : frame.x - (frame.optimizedMatWidth + frame.optimizedFrameWidth) * 2,
				height : frame.y - (frame.optimizedMatWidth + frame.optimizedFrameWidth) * 2,
				'line-height' : frame.y - (frame.optimizedMatWidth + frame.optimizedFrameWidth) * 2 + 'px'
			});

			/*this.$('.progress-indicator').css({
				'bottom': ( $(window).height() - $('.frame-container').offset().top - frame.y ) / 2
			});*/
			var frameSize = this.model.getActualFrameSize();
			this.$('.show-width span').html(frameSize.x + ' &quot;');
			this.$('.show-height span').html(frameSize.y + ' &quot;');

			var boxShadow = this.renderFrameAndMatAndShadows({
			    "matWidth": frame.matWidth,
			    "matColorValue": frame.matColorValue,
			    "matInnerShadedBevelColor": "#444",
			    "matInnerLitBevelColor": "#ddd",
			    "hue": frame.hue,
			    "saturation": frame.saturation,
			    "topLightness": frame.topLightness,
			    "sideLightness": frame.sideLightness,
			    "optimizedFrameWidth": frame.optimizedFrameWidth,
			    "optimizedMatWidth": frame.optimizedMatWidth,
			    "outerShadows": "5px 25px 60px 0px",
			    "scaleDownShadows": 0,
			});
			this.$('.frame-shadow').css('box-shadow', boxShadow);
			this.$('.frame-container,.frame-shadow').addClass('show');
			if(this.model.get('image_path')){
				this.$('.frame').addClass('has-image');
				this.$('.frame').css('background-image', 'url("' + this.model.get('image_path') + '")');
			}
		},
		selectImage: function(e){
			this.$('.file-input').trigger('click');
		},
		fileSelected: function(e){
			this.$('.frame').addClass('has-image');
			var fr = new FileReader();
			  // when image is loaded, set the src of the image where you want to display it
			fr.onload = _.bind(function(evt) {
				var image = new Image();
			    image.src = evt.target.result;
			    image.onload = _.bind(function() {
			        // access image size here
			        console.log(image.width);
			        this.imageLoadedWithDimension(image.width, image.height);
			        this.$('.frame').css('background-image', 'url("' + evt.target.result + '")');
			        // $('#imgresizepreview, #profilepicturepreview').attr('src', this.src);
			    }, this);
				// this.$('.frame').css('background-image', 'url("' + evt.target.result + '")');
			}, this);
			fr.readAsDataURL(e.target.files[0]);
		},
		renderFrameAndMatAndShadows: function(t) {
		    var shadowColor = "#000";
			var boxShadow = "1px 2px 5px hsla(0,0%,0%,1), ";
		    var getTotalMatAndShadowWidth = function (t){return t>0?t+2:0};
			var totalMatAndShadowWidth = getTotalMatAndShadowWidth(t.optimizedMatWidth) - t.scaleDownShadows;
			var topBottomColor = "hsla(" + t.hue + ", " + t.saturation + "%, " + t.topLightness + "%, 1)";
			var leftRightColor = "hsla(" + t.hue + ", " + t.saturation + "%, " + t.sideLightness + "%, 1)";
			for ( var i = 1; i <= t.optimizedFrameWidth; i++)
				if( i == t.optimizedFrameWidth) {
					boxShadow += "inset 0 " + (i - t.scaleDownShadows) + "px 4px 1px " + shadowColor +
									", inset " + (i - t.scaleDownShadows) + "px 4px 1px 1px " + shadowColor +
									", inset 0 -" + i + "px 2px " + shadowColor +
									", inset -" + i + "px 0 0 " + shadowColor +
									", " + t.outerShadows + " hsla(0,0%,0%,.25)" +
									", inset " + i + "px " + i + "px 0px " + t.optimizedMatWidth + "px " + t.matColorValue +
									", inset -" + i + "px -" + i + "px 0px " + t.optimizedMatWidth + "px " + t.matColorValue;
					if (t.matWidth > 0){
						boxShadow += ", inset -" + i + "px -" + i + "px 0px " + totalMatAndShadowWidth + "px " + t.matInnerLitBevelColor +
										", inset " + i + "px " + i + "px 1px " + totalMatAndShadowWidth + "px " + t.matInnerShadedBevelColor
					}
				}else{
					topBottomColor = "hsla(" + t.hue + ", " + t.saturation + "%, " + (t.topLightness + i / 3) + "%, 1)";
					leftRightColor = "hsla(" + t.hue + ", " + t.saturation + "%, " + (t.sideLightness + i / 3) + "%, 1)";
					boxShadow += "inset 0 " + i + "px 0 " + topBottomColor +
									", inset " + i + "px 0 0 " + leftRightColor +
									", inset 0 -" + i + "px 0 " + topBottomColor +
									", inset -" + i + "px 0 0 " + leftRightColor + ",";
				}
			return boxShadow;
		},
		changeWallColor: function(color){
			this.model.set({'wall_color': color},{silent:true});
			this.$('.wall').css('background-color', color);
		},
		imageLoadedWithDimension: function(width, height){
			this.$('.step li').removeClass('active');
			this.$('.step li:eq(1)').addClass('active');
			$('.collapse-link:not(.open)').trigger('click');
			this.model.set({imageDimensionWidth: width, imageDimensionHeight: height});
			var ratio = width/height,
				actualPrintSize = {
					x : this.model.get('imageWidth'),
					y : this.model.get('imageHeight')
				},
				minPPI = 100,
				maxWidth = Math.round(width / minPPI),
				maxHeight = Math.round(height / minPPI),
				sizes;
			if(maxWidth < actualPrintSize.x || maxHeight < actualPrintSize.y){
				// Give PPI Warning
				sizes  = [
					{
						x : Math.round(width / minPPI),
						y : Math.round(height / minPPI)
					}
				];
				this.showRecommendedSizes(sizes, true);
			}else{
				// Check for aspect ratio
				sizes = [];
				var expected = this.putInBoundingBox(actualPrintSize.x, Math.round(actualPrintSize.x/ratio), maxWidth, maxHeight);
				if( actualPrintSize.y !== expected.y || actualPrintSize.x !== expected.x ){
					sizes.push(expected);
				}
				expected = this.putInBoundingBox(Math.round(actualPrintSize.y * ratio), actualPrintSize.y, maxWidth, maxHeight);
				if( actualPrintSize.y !== expected.y || actualPrintSize.x !== expected.x ){
					sizes.push(expected);
				}
				this.showRecommendedSizes(sizes, false);
			}
		},
		putInBoundingBox: function(x, y, maxWidth, maxHeight){
			var ratio = x/y;
			if( x > maxWidth ){
				return {
					x : maxWidth,
					y : Math.round(maxWidth / ratio)
				};
			}
			if( y > maxHeight){
				return {
					x : Math.round(maxHeight * ratio),
					y : maxHeight
				};
			}
			return {x : x, y : y};
		},
		dissmissNotification: function(e){
			e.preventDefault();
			e.stopPropagation();
			$('.notification').removeClass('visible');
		},
		showRecommendedSizes: function(sizes, lowPPIWarning){
			if(sizes.length){
				var message = "As per the asphect ratio of selected image, following sizes are more approprite";
				if(lowPPIWarning){
					message = "Oops! quality of this selected image is not sufficient for getting print of selected size.";
				}
				$('.notification .main-message').html(message);
				$('#recommended-sizes').empty();
				_.each(sizes, function(size){
					$('<a />', {
						html : size.x + 'x' + size.y,
						className : 'size-link',
						click: _.bind(this.chooseSize, this),
						data : {
							size : size
						}
					}).appendTo($('#recommended-sizes'));
				}, this);
				$('.notification').addClass('visible');
			}
		},
		chooseSize: function(e){
			e.preventDefault();
			e.stopPropagation();
			var size = $(e.target).data('size');
			$('.notification').removeClass('visible');
			this.model.set({
				imageWidth : size.x,
				imageHeight : size.y
			});
			this.model.trigger('lock-orientation');
		}
	});

	return PreviewView;
});