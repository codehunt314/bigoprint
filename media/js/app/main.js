

var renderFrameAndMatAndShadows = function(t) {
    var shadowColor = "#000";
	var boxShadow = "1px 2px 5px hsla(0,0%,0%,1), ";
    var getTotalMatAndShadowWidth = function (t){return t>0?t+2:0};
	var totalMatAndShadowWidth = getTotalMatAndShadowWidth(t.optimizedMatWidth) - t.scaleDownShadows;
	var topBottomColor = "hsla(" + t.hue + ", " + t.saturation + "%, " + t.topLightness + "%, 1)";
	var leftRightColor = "hsla(" + t.hue + ", " + t.saturation + "%, " + t.sideLightness + "%, 1)";
	for ( i = 1; i <= t.optimizedFrameWidth; i++)
		if( i == t.optimizedFrameWidth) {
			boxShadow += "inset 0 " + (i - t.scaleDownShadows) + "px 4px 1px " + shadowColor +
							", inset " + (i - t.scaleDownShadows) + "px 4px 1px 1px " + shadowColor +
							", inset 0 -" + i + "px 2px " + shadowColor +
							", inset -" + i + "px 0 0 " + shadowColor +
							", " + t.outerShadows + " hsla(0,0%,0%,.25)" +
							", inset " + i + "px " + i + "px 0px " + t.optimizedMatWidth + "px " + t.matColorValue +
							", inset -" + i + "px -" + i + "px 0px " + t.optimizedMatWidth + "px " + t.matColorValue;
			if (t.matWidth > 0){
				boxShadow += ", inset -" + i + "px -" + i + "px 0px " + totalMatAndShadowWidth + "px " + t.matInnerLitBevelColor +
								", inset " + i + "px " + i + "px 1px " + totalMatAndShadowWidth + "px " + t.matInnerShadedBevelColor
			}
		}else{
			topBottomColor = "hsla(" + t.hue + ", " + t.saturation + "%, " + (t.topLightness + i / 3) + "%, 1)";
			leftRightColor = "hsla(" + t.hue + ", " + t.saturation + "%, " + (t.sideLightness + i / 3) + "%, 1)";
			boxShadow += "inset 0 " + i + "px 0 " + topBottomColor +
							", inset " + i + "px 0 0 " + leftRightColor +
							", inset 0 -" + i + "px 0 " + topBottomColor +
							", inset -" + i + "px 0 0 " + leftRightColor + ",";
		}
	return boxShadow;
};


var boxShadow = renderFrameAndMatAndShadows({
    // "artWidth": 36,
    // "artHeight": 24,
    // "frameWidth": 1.25,
    // "frameWidthId": 2,
    // "rabbet": 0.25,
    "matWidth": 1,
    // "matColor": "Off-White mat",
    // "matColorId": 1,
    "matColorValue": "hsla(0, 0%, 100%, 1)",
    "matInnerShadedBevelColor": "#444",
    "matInnerLitBevelColor": "#ddd",
    // "profile": "Natural Walnut",
    // "profileId": "5",
    // "cornerSlug": "black",
    "hue": 9,
    "saturation": 9,
    "topLightness": 16,
    "sideLightness": 13,
    // "borderColors": "#91554b #91554b",
    // "grainValue": 2,
    "optimizedFrameWidth": 10,
    "optimizedMatWidth": 11,
    // "optimizedExteriorHeight": 314,
    // "optimizedExteriorWidth": 446,
    // "exteriorHeight": 26,
    // "exteriorWidth": 38,
    // "glaze": "Standard",
    // "frameColorPrice": 25,
    // "framePricePerLinearInch": 0.49,
    // "glazePricePerSquareInch": 0.05,
    // "hasEditorControls": 1,
    // "imagePath": "",
    // "printPrice": 0,
    "outerShadows": "5px 25px 60px 0px",
    // "maxExteriorWidthInPixels": 470,
    // "maxExteriorHeightInPixels": 600,
    // "setFrameToDivWidth": ".zen_content",
    // "upperWindowWidthTrigger": 700,
    // "lowerWindowWidthTrigger": 0,
    "scaleDownShadows": 0,
    // "i": 100,
    // "x": 1
});

$(document).ready(function(){
    $('.frame-container').css('box-shadow', boxShadow);
    $('.frame-width').jRange({
        from: 0.75,
        to: 2.00,
        step: 0.25,
        scale: ["0.75","1.00","1.25","1.50","1.75","2.00"],
        format: '%s',
        width: 340,
        showLabels: true,
        snap: true
    });
    $('.mat-width').jRange({
        from: 0.75,
        to: 2.00,
        step: 0.25,
        scale: ["0.75","1.00","1.25","1.50","1.75","2.00"],
        format: '%s',
        width: 340,
        showLabels: true,
        snap: true
    });
});
