from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'orderapp.views.home', name='home'),
    url(r'^try/$', 'orderapp.views.try_home', name='try'),
    url(r'^faq/$', TemplateView.as_view(template_name='static/faq.html')),
    url(r'^terms/$', TemplateView.as_view(template_name='static/terms.html')),
    url(r'^pricing/$', TemplateView.as_view(template_name='static/pricing.html')),
    url(r'^contact-us/$', TemplateView.as_view(template_name='static/contact.html')),
    url(r'^feedback/$', 'customer.views.feedback', name='feedback'),
    url(r'^checkout/$', 'orderapp.views.checkout', name='checkout'),
    url(r'^orders/$', 'orderapp.views.orders_list', name='orders_list'),
    url(r'^order/(?P<orderid>[A-Z0-9]+)/$', 'orderapp.views.order_detail', name='order_detail'),
    url(r'^order/(?P<orderid>[A-Z0-9]+)/edit/$', 'orderapp.views.order_edit', name='order_edit'),
    url(r'^order/(?P<orderid>[A-Z0-9]+)/print/$', 'orderapp.views.order_print', name='order_print'),
    url(r'^order/(?P<orderid>[A-Z0-9]+)/confirm/$', 'orderapp.views.order_confirm', name='order_confirm'),
    url(r'^order/(?P<orderid>[A-Z0-9]+)/cancel_payment/$', 'orderapp.views.cancel_payment', name='cancel_payment'),
    url(r'^order/(?P<orderid>[A-Z0-9]+)/makepayment/$', 'orderapp.views.makepayment', name='makepayment'),
    url(r'^customer/login/$', 'customer.views.customer_login', name='customer_login'),
    url(r'^customer/logout/$', 'customer.views.customer_logout', name='customer_logout'),
    url(r'^customer/register/$', 'customer.views.customer_registration', name='customer_registration'),
    url(r'^customer/add_address/$', 'customer.views.add_address', name='add_address'),
    url(r'^customer/forgot_password/$', 'customer.views.forgot_password', name='forgot_password'),
    url(r'^customer/reset_password/(?P<uidb64>.+)/(?P<token>.+)/$', 'customer.views.reset_password', name='reset_password'),
    url(r'^customer/edit_address/(?P<addressid>[A-Z0-9]+)/$', 'customer.views.edit_address', name='edit_address'),
    url(r'^customer/delete_address/(?P<addressid>[A-Z0-9]+)/$', 'customer.views.delete_address', name='delete_address'),
    url(r'^admin_dashboard/$', 'customer.views.admin_dashboard', name='admin_dashboard'),
    url(r'^admin_setstatus/$', 'customer.views.admin_set_status', name='admin_set_status'),
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

